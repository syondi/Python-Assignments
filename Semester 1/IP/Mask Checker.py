# This program was created for the Purposes of EAL Semester 1 Networking - Programming.
    # Created by Kyle Mcshane @ kylemcshane1994@gmail.com at 19/1/2018 using Python 3.6(64-bit).
        # The purpose of this program is to validate an IP address that the user inputs.
        # If it is valid and exists the program also displays the Subnet Mask.

import ipaddress
# This module helps and handles with the manipulation of ip addresses
def main(): 
    print("------------------------------------------------------------------")
    ip_address = input("Please insert an IPv4 address: - mask is optional : ")
    print("------------------------------------------------------------------")
    netmask_ipv4 = ip_address_checker(ip_address)
    print("Netmask : [",ip_address[-3:],"] =", netmask_ipv4.netmask)
    
    main()

def ip_address_checker(ip):
    
    # Imports the ip adress module and checks it against the ipv4 function
    # uses exception handling to determine wether the ip is valid or not
   
    try :
        ipaddress.ip_network(ip)
        # Removes the final three digits from the IP, eg [/30] to display the ip range
        print(ip[:-3]," is a valid ip address ! ") 
        # Passes the IP into an imported function ip_network to return the network range
        netmask_ipv4 = ipaddress.ip_network(ip)
        # We return the result to the main function
        return netmask_ipv4
    except ValueError:
        print(ip," is not a valid ip address !")
        main()
    print("------------------------------------------------------------------ \n")
main()
