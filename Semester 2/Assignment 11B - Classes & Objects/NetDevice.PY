# This program was created on 04/02/2018 for the purposes of [Semeser 2 - Programming].
# [Pearson] - Starting Out with Python, 2nd ed.- [Malestrom].
# Created by Kyle Mcshane @ kylemcshane1994@gmail.com.

class NetDevice:
    # Docstring that will provide information for debugging in the help function.
    """This class is for storing basic information about a netdevice such as Name,MAC,IP,Mask & Gateway Information"""

    # This is where the class initializes and all the attributes are assigned to the object.
    def __init__(self,name,macAddress,ipAddress,mask,gateway):
        self.__name = name
        self.__macAddress = macAddress
        self.__ipAddress = ipAddress
        self.__mask = mask
        self.__gateway = gateway

    # Values for the varibles of the object will be stored here.
    def set_name(self,name):
        """Setting the device name"""
        self.__name = name
    def set_macAddress(self,macAddress):
        """Setting the MAC"""
        self.__macAddress = macAddress
    def set_ipAddress(self,ipAddress):
        """Setting the IP"""
        self.__ipAddress = ipAddress
    def set_mask(self,mask):
        """Setting the mask"""
        self.__mask = mask
    def set_gateway(self,gateway):
        """Setting the gateway"""
        self.__gateway = gateway

    # Here is the part of the class where we return the values we have already set in the class.
    def get_name(self):
        """Returns the device name"""
        return self.__name
    def get_macAddress(self):
        """Returns the devices MAC Address"""
        return self.__macAddress
    def get_ipAddress(self):
        """Returns the devices IP Address"""
        return self.__ipAddress
    def get_mask(self):
        """Returns the devices mask information"""
        return self.__mask
    def get_gateway(self):
        """Returns the devices default gateway information"""
        return self.__gateway
# Will provide useful information for this class when called by the user.