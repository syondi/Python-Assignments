# This program was created on 04/02/2018 for the purposes of [Semeser 2 - Programming].
# [Pearson] - Starting Out with Python, 2nd ed.- [Malestrom].
# Created by Kyle Mcshane @ kylemcshane1994@gmail.com.
# The purpose of this program is to store relevant information about NetDevices within a class.
# Varibles such as Name,MAC,IP,Mask & Gateway will be stored about each device in their own unique object.

import NetDevice, os
# Here we are importing the special NetDevice class i made for this module.

def main():
    # Common varibles will be used through-out the program we intialize them here.
    tick=1
    device_list = []
    devices = input_menu(tick,device_list)

def input_menu(tick,device_list):
    
    print(" DEVICE : ", tick,"\n","="*25)
    name = str(input(" * Device Name : "))
    mac = str(input(" * Device MAC : "))
    ip = str(input(" * Device IP : "))
    mask = str(input(" * Device Mask : "))
    gateway = str(input(" * Device Gateway : "))
    print("\n")

    # Here we move the varibles we just recieved into the classes _init_ arguements
    device = NetDevice.NetDevice(name,mac,ip,mask,gateway)
    # Now we pass these varibles into a list
    device_list.append(device)
    # Now the program heads to the exit prompt each time input is ran, this allows for infinate input, albiet the menu can be annoying for fast input.
    exit_prompt(tick,device_list)
    
   
   
  

def exit_prompt(tick,device_list): 
    """ Simple Function that exsists to allow the user to stop inputting data """

    user_prompt = input("Display the data now? [Y/N] : ")
    if user_prompt == ("Y") or user_prompt == ("y"):
       os.system('cls') # Clears the Screen
       display_class(tick,device_list)
    elif user_prompt == ("N") or user_prompt == ("n"):
       os.system('cls') # Clears the screen
       tick+=1
       input_menu(tick,device_list)
    else: 
        print("Invalid input please try again (Y/N EXPECTED) /n")
        exit_prompt(tick,device_list)

def display_class(tick,device_list):
    # Function that iterates over all the items in the list, counter is used to keep track of the current iteration of loop for the item ID in the class.
    counter = 1
    for device in device_list:
        print("  Device Number : ", counter,"of", tick, "\n", "-"*25)
        print("  * NAME : ",device.get_name())
        print("  * MAC : ",device.get_macAddress())
        print("  * IP : ",device.get_ipAddress())
        print("  * MASK : ",device.get_mask())
        print("  * Gateway : ",device.get_gateway())
        print("-"*25)
        input("  \n Press Enter for next item...")
        os.system('cls')
        counter+=1

 

    
main()