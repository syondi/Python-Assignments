#This program was created on 21/04/2018 for the purposes of [Semetser 2 - Programming]
# [Pearson] - Starting Out with Python, 2nd ed.- [Malestrom]
# Created by Bo Mikkelsen and Kyle Mcshane
# contact information boxx1483@edu.eal.dk, Kyle0055@edu.eal.dk
# The purpose of this program is to show the usage of the python module SQLite3
#version 2.1


import sqlite3
import subprocess
import tkinter.scrolledtext as tkst
import tkinter





class SQliteGUI():
##########################################################################################
# Placement and main loop of the GUI aswell as adding widgets                            #
##########################################################################################
    def __init__(self):
        self.master = tkinter.Tk()  # Create master widget object
        self.dataBaseToWorkOn = 'Kyle1'
        # Connect to database
        self.conn = sqlite3.connect(self.dataBaseToWorkOn)
        self.cursor = self.conn.cursor()
        # Create the GUI widgets: Buttons, labels and text fields
        self.tableLabel = tkinter.Label(self.master, text="Enter table: ")
        self.listAllButton = tkinter.Button(self.master,
                                            text='List table content',
                                            command=self.listAllCustomers)
        self.insertNewCustomerButton = tkinter.Button(self.master,
                                                      text='Insert new customer',
                                                      command=self.insertNewCustomer)
        self.bashButton = tkinter.Button(self.master,text='Run in bash'
                                         ,command = self.callmain,height = 5, width = 10,
                                         bg='white',font=('times','12'))

        self.resultSet = tkst.ScrolledText(self.master, undo=True,
                                           wrap='word',  # wrap text at full words
                                           width=80,  # characters
                                           height=7,  # text lines
                                           bg='orange')

        self.quitButton = tkinter.Button(self.master,
                                         text='Quit',
                                         command=self.master.destroy)
        # Create the GUI widgets: Entry fields
        self.tableEntry = tkinter.Entry(self.master)
        self.tableEntry.insert(0, 'kyletable')  # Insert a default text
        self.newId = tkinter.Entry(self.master)
        self.newId.insert(0, 'Id')  # Insert a default text
        self.newName = tkinter.Entry(self.master)
        self.newName.insert(0, 'Name')  # Insert a default text
        self.newEmail = tkinter.Entry(self.master)
        self.newEmail.insert(0, 'Email')  # Insert a default text
        self.newCityId = tkinter.Entry(self.master)
        self.newCityId.insert(0, 'City Id')  # Insert a default text
        # Organize widgets in rows and columns on the grid:
        self.tableEntry.grid(row=0, column=1, sticky="W")
        self.listAllButton.grid(row=2, column=0)
        self.bashButton.grid(row=3, column=4)
        self.quitButton.grid(row=4, column=3)
        self.tableLabel.grid(row=0, sticky="W")
        self.resultSet.grid(row=1, column=0, columnspan=4)
        self.insertNewCustomerButton.grid(row=4, column=0, columnspan=1)
        self.newId.grid(row=3, column=0, columnspan=1)
        self.newName.grid(row=3, column=1, columnspan=1)
        self.newEmail.grid(row=3, column=2, columnspan=1)
        self.newCityId.grid(row=3, column=3, columnspan=1)
        self.master.mainloop()


##########################################################################################
#Listing all rows into the scrolling text within the GUI                                 #
##########################################################################################

    def listAllCustomers(self):
        sqlQuery = 'SELECT * FROM ' + str(self.tableEntry.get())
        print(sqlQuery)
        self.cursor.execute(sqlQuery)
        self.conn.commit()
        columns = ''  # Initialise row holder
        # Pack columns and rows into a string
        for row in self.cursor.fetchall():
            for field in row:
                columns += str(field) + '\t\t'
        # columns += '\n'
        print(columns)
        self.resultSet.insert('insert', columns)

    def insertNewCustomer(self):
        customerId = self.newId.get()
        name = self.newName.get()
        email = self.newEmail.get()
        address = 'bla bla'
        city = self.newCityId.get()
        personData = [customerId, name, email, address, city]
        self.insertCustomer(personData)

##########################################################################################
#Function that takes userdefined input, and add it to the table                          #
##########################################################################################
    def insertCustomer(self, customerData):
        self.cursor.execute("""
        INSERT INTO kyletable (idCust, name, email,address, city)
        VALUES (?, ?, ?, ?,?)""",customerData)
        self.conn.commit()

    def callmain(self):
        main()
class bcolors:
    OKGREEN = '\033[92m'
    ENDC = '\033[0m'
    OKBLUE = '\033[94m'
    BOLD = '\033[1m'



##########################################################################################
#Promts user for DB to create or work on                                                 #
##########################################################################################
def main():
    SQLDB = str(input(bcolors.OKGREEN + 'Type a Database name:' + bcolors.ENDC))
    try:
        conn = sqlite3.connect(SQLDB)
    except Exception as somethingwentwrong:
        print(somethingwentwrong)
        main()
    else:
        cursor = conn.cursor()
        main_menu(conn,SQLDB,cursor)


##########################################################################################
#Displaying Main menu of options                                                         #
##########################################################################################
def main_menu(conn,SQLDB,cursor):

        print(bcolors.BOLD + '-'*25,'\n',
              'Working on DB:', SQLDB, '\n',
              'Main menu','\n',
              '-'*25,'\n',
              '1. Show existing tables','\n',
              '2. Create a new table','\n',
              '3. Work on existing table','\n',
              '4. Update a table','\n',
              '5. Delete a Row in a table','\n',
              '6. Choose another database','\n',
              '7. Display contents of a  table','\n',
              '8. Display GUI again.','\n',
              '9. Exit the program','\n', '-'*25 + bcolors.ENDC )
        ##########################################################################################
        #decisions of the user in mainmenu                                                       #
        ##########################################################################################

        user = str(input(bcolors.OKBLUE + 'press a number:' + bcolors.ENDC))
        if user == '1':
            show_tables(conn,SQLDB,cursor)
        elif user == '2':
            create_table(conn,SQLDB,cursor)
        elif user == '3':
            insert_table(conn,SQLDB,cursor)
        elif user == '4':
            update_table(conn,SQLDB,cursor)
        elif user == '5':
            delete_row(conn,SQLDB,cursor)
        elif user == '6':
            main()
        elif user == '7':
            display_table(conn,SQLDB,cursor)
        elif user == '8':
            SQliteGUI()
        elif user == '9':
            exit()
        else:
            print('Typo, try again')
            main_menu(conn,SQLDB,cursor)


##########################################################################################
#Function that shows current tables in the DB                                            #
##########################################################################################
def show_tables(conn,SQLDB,cursor):

    print('Showing existing tables...')
    print('-'*25)
    subprocess.call(["sqlite3",SQLDB,".tables"])
    print('-'*25)
    main_menu(conn,SQLDB,cursor)

##########################################################################################
#Creating a table with hardcoded columns                                                 #
##########################################################################################
def create_table(conn,SQLDB,cursor):
    # creating a table
    table = str(input('Enter a table name:'))
    try:
        cursor.execute("""CREATE TABLE %s
            (   idCust integer primary key,
                name varchar,
                email varchar,
                address varchar,
                city integer)
            """ % table)

    except Exception as TableError:
        print(TableError)
        create_table(conn,SQLDB, cursor)
    else:
        conn.commit()
        main_menu(conn,SQLDB,cursor)

##########################################################################################
#Promts user for values to input into a user defined table                               #
##########################################################################################
def insert_table(conn,SQLDB, cursor):
    table = str(input('Enter table to work on: '))
    name = str(input('Enter a name: '))
    email = str(input('Enter a email: '))
    address = str(input('Enter a address: '))
    postcode = int(input('Enter a postcode: '))

    cursor.execute("""
    INSERT INTO %s (name, email, address, city)
    VALUES ('%s', '%s', '%s', %d)"""% (table, name, email, address, postcode))
    conn.commit()
    print('Content has been inserted')
    main_menu(conn,SQLDB, cursor)


##########################################################################################
#Displaying contents of user defined table                                                            #
##########################################################################################
def display_table(conn,SQLDB, cursor):
    table = str(input('Enter a table to display: '))

    cursor.execute("""
    SELECT '%s'.idCust, '%s'.name, '%s'.email, '%s'.address,
     '%s'.city FROM '%s' """% (table, table, table, table, table, table))
    conn.commit()
    # Displaying rows in the table

    print('this is the content of the table')
    for row in cursor.fetchall():
        print("Row:", end = '')
        for field in row:
            print(field, ' ', end='')
        print('')

    print('-' * 25)
    main_menu(conn,SQLDB,cursor)


##########################################################################################
#  Update a row in a user defined table                                                  #
##########################################################################################
def update_table(conn,SQLDB,cursor):
    table = str(input('Enter a table:'))
    print('Updating the table:')
    row = int(input('Enter which row should be updated:'))
    name = str(input('Enter a new name:'))
    email = str(input('Enter a new email:'))
    address = str(input('Enter a new address:'))
    postcode = int(input('Enter a new postcode:'))
    cursor.execute("""
    UPDATE '%s'  SET name = '%s', email = '%s', address = '%s', city = %d
    WHERE idCust = %d """% (table, name, email, address, postcode, row))
    conn.commit()
    main_menu(conn,SQLDB,cursor)

##########################################################################################
# Deleting a current row                                                                  #
##########################################################################################
def delete_row(conn,SQLDB,cursor):
    print('Deleting values')
    table = str(input('Enter a table:'))
    row = int(input('Enter which row should be deleted:'))
    cursor.execute("""
    DELETE FROM '%s' WHERE idCust = %d """% (table,row))
    conn.commit()
    main_menu(conn,SQLDB,cursor)


SQliteGUI()
